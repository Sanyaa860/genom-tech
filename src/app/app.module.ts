import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MasternodesComponent } from './masternodes/masternodes.component';
import { ExchangesComponent } from './exchanges/exchanges.component';

@NgModule({
  declarations: [
    AppComponent,
    MasternodesComponent,
    ExchangesComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
