import { Component, OnInit } from '@angular/core';
import { MasternodesService } from '../services/masternodes.service';

@Component({
  selector: 'app-masternodes',
  templateUrl: './masternodes.component.html',
  styleUrls: ['./masternodes.component.css']
})

export class MasternodesComponent implements OnInit {
  mns: any;

  constructor(private masternodesService: MasternodesService) { }

  ngOnInit() {
    this.mns = this.masternodesService.getMasternodes().subscribe(resp => {
      this.mns = resp;
    });
  }

}
