import { Component, OnInit } from '@angular/core';
import { ExchangesService } from '../services/exchanges.service';

@Component({
  selector: 'app-exchanges',
  templateUrl: './exchanges.component.html',
  styleUrls: ['./exchanges.component.css']
})
export class ExchangesComponent implements OnInit {
  market: any;

  constructor(private exchangesService: ExchangesService) { }

  ngOnInit() {
    this.market = this.exchangesService.getMarkets().subscribe(resp => {
      this.market = resp;
    });
  }

}
