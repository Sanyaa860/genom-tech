import { TestBed } from '@angular/core/testing';

import { MasternodesService } from './masternodes.service';

describe('MasternodesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MasternodesService = TestBed.get(MasternodesService);
    expect(service).toBeTruthy();
  });
});
