import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExchangesService {
  coingeckoUrl = 'https://api.coingecko.com/api/v3/coins/genom';

  constructor(private http: HttpClient) { }

  getMarkets(): Observable<any> {
    return this.http.get(this.coingeckoUrl)
  }
}
