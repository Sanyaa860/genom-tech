import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MasternodesService {
  masternodesUrl = 'https://api.genom.tech/v1/masternodes';

  constructor(private http: HttpClient) { }

  getMasternodes(): Observable<any> {
    return this.http.get(this.masternodesUrl)
  }
}
