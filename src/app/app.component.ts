import { Component } from '@angular/core';
import { faSpaceShuttle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'genom-tech';
  faSpaceShuttle = faSpaceShuttle;
}
